import Headings from '@narative/gatsby-theme-novela/src/components/Headings'
import Layout from '@narative/gatsby-theme-novela/src/components/Layout'
import Section from '@narative/gatsby-theme-novela/src/components/Section'
import SEO from '@narative/gatsby-theme-novela/src/components/SEO'
import React from 'react'

function NotFoundPage() {
  return (
    <Layout>
      <SEO />
      <Section>
        <div style={{ marginTop: '100px' }}>
          <Headings.h1>404: Page Not Found</Headings.h1>
          {/* <!-- Begin: HubSpot Academy - Digital Advertising Badge --> */}
          <div class="academy-badge">
            <a
              href="http://academy.hubspot.com/certification"
              title="Digital Advertising"
            >
              <img src="https://hubspot-academy.s3.amazonaws.com/prod/tracks/user-badges/13467079/c012cc94f8fa41abbdae8c65e2586997-1615445695903.png" />
            </a>
          </div>
          {/* <!-- End: HubSpot Academy - Digital Advertising Badge --> */}
        </div>
      </Section>
    </Layout>
  )
}

export default NotFoundPage

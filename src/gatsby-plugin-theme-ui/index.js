import novelaTheme from '@narative/gatsby-theme-novela/src/gatsby-plugin-theme-ui';

export default {
  ...novelaTheme,
  // initialColorMode: `dark`,
  colors: {
    ...novelaTheme.colors,
    accent: '#21AB74',
    progress: '#21AB74',
    modes: {
      dark: {
        ...novelaTheme.colors.modes.dark,
        accent: "#FB3E77",
        progress: "#FB3E77",
      }
    }
  },
};
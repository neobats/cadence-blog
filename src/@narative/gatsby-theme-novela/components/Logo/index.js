import React from 'react';
import Cadence from './CadenceLogo';

const Logo = ({ fill }) => 
  (
    <Cadence w="225" h="107" fill={fill} />
  )

export default Logo
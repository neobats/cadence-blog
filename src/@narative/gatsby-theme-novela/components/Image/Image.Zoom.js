import Figcaption from '@narative/gatsby-theme-novela/src/components/Figcaption'
import React, { useCallback, useState } from 'react'
import { Controlled as ControlledZoom } from 'react-medium-image-zoom'
import 'react-medium-image-zoom/dist/styles.css'
import { useThemeUI } from 'theme-ui'

const ImageZoom = props => {
  const [isZoomed, setIsZoomed] = useState(false)
  const { theme } = useThemeUI()

  const image = {
    ...props,
    className: 'Image__Zoom',
    style: {
      display: 'block',
      margin: '0 auto',
      width: '100%',
      borderRadius: isZoomed ? '5px' : '0px',
    },
  }

  const handleZoomChange = useCallback(shouldZoom => {
    setIsZoomed(shouldZoom)
  }, [])

  return (
    <ControlledZoom
      isZoomed={isZoomed}
      onZoomChange={handleZoomChange}
      zoomMargin={40}
      overlayBgColorEnd={theme.colors.background}
    >
      <img
        className={image.className}
        src={image.src}
        alt={image.alt}
        style={image.style}
      />
      {isZoomed && <Figcaption>{image.alt}</Figcaption>}
    </ControlledZoom>
  )
}

export default ImageZoom

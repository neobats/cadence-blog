---
title: 🤝 Building Relationships through Email Marketing
author: Guy Batton
date: 2021-03-25
hero: ./images/hero.jpg
excerpt:  From first impressions to love letters, email marketing can help you build customer relationships that last a lifetime.
---

When I first met my wife, I didn’t ask for her number, I got her email address. You may think that we had a rocky start if that’s how we began talking (you’d be right) but it ended up being a lot better for our relationship, I think.

Crafting a good email takes work, attention, and care. You can get more across in a well-worded email than you can in a text. But of course, no email is a replacement for an in-person conversation, so we used email to schedule dates, and those blossomed into a beautiful marriage.

My story is probably not the standard for relationships, but it’s far from unique. Email is one of the most common ways to find success in marketing. But in its popularity, it can feel like a charade, scam, or annoyance if not done well. But the sure way to success in email marketing is to treat it like a relationship. You need to nurture it just like a teen texting their significant other.

> The sure way to success in email marketing is to treat it like a relationship.  

If you want success in email marketing, here are some tips to effectively build relationships with your customers:

1. Earn the right to talk
2. Take each opportunity as a gift

The best marketing is a conversation. It’s like building two successful lives. It’s a relationship. You get to work with your customer to build a better future for both of them. Take time to talk while you build. Done right, email can be your invitation to pitch a tent and stay a while in their life.

<div class="Image__Small">
  <img
    src="./images/tent.jpg"
    title="Two People Building a tent"
    alt="Two people building a tent together.. Photo by Vanessa Garcia on Pexels"
  />
</div>

*Hey, if you’re still doubting, here’s a statistic for you from [Mailchimp](https://mailchimp.com/email-marketing/)*:

Email has a good return on investment; one study in 2015 found that the ROI for email was $38 per $1 spent (USD). As the folks at Mailchimp put it, “When shoppers are ready to buy something, they often look for emails from their favorite stores.”

## Earn the Right to Talk

Like any relationship, one doesn’t automatically get the right to say whatever they want to the other person. Building trust [takes time](https://www.cadence.surf/growth-in-time-how-blogging-is-like-gardening), and you have to start smaller before you can mine the depths of a rich relationship.

In marketing this is usually the top of the [funnel](https://www.salesforce.com/ap/blog/2020/06/what-is-a-sales-funnel.html). There are a number of ways to get your customer in that funnel. But if we’re building a relationship, the _means_ doesn’t matter nearly as much as the _manner_.

> Someone will sign up for emails because of a good first impression.  

So here are some ideas on how to start your customer relationship off on the right foot:

* Give away something great. No, like, [really great](https://www.garyvaynerchuk.com/why-you-shouldnt-charge-for-your-best-work/). Just trade an email for it.
* Show your best self from the start, and and keep it that way. Then drop a link to sign up on your website.
* Build a brand people want to talk to.

More than likely, someone will find you and sign up for emails because of a good first impression. But a relationship doesn’t last long off of a good first impression. You have to build on it.

Earning the right to speak is only the start of the journey, you also have to…

## Take Each Opportunity as a Gift

Success can be defined a bunch of different ways, but in a world where someone may get 300 marketing emails a day, I think success can look like staying in the inbox.

That’s another way of saying that every email your customer reads is a gift to you.

And if you want to build a good relationship with them, one great way to do that is to say thanks. It may sound cliche to put it that way, but good manners goes a long way. Of course, saying thanks doesn’t always require words. Sometimes actions will be better.

Mailchimp puts this really well (they also do this really well in practice!):
“People who subscribe to your list are so interested in what you have to say that they’re willing to invite you into their inbox. This is a privilege. Honor it by letting them be the first to know about new products and sales. Or, go one step further like the company Oui Shave  that asks its best customers to participate in product surveys and rewards them by making them beta testers for new products.” [Email Marketing Field Guide](https://mailchimp.com/resources/email-marketing-field-guide/)

## Takeaways

Email marketing is, at its core, no different than other marketing. Every avenue is a tool to better build relationships with your customers.

But if you play your cards right, your email strategy can work out like mine did with my wife: a happy relationship built for a lifetime.

> Treat your subscribers like VIPs—Mailchimp  

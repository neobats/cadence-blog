---
title: 🧠 Leveraging UX Psychology for Social Media Advertising
author: Guy Batton
date: 2021-04-15
hero: ./images/hero.jpg
excerpt:  How can you get into the head of your audience? How can you know what they look for? How can you get their attention? As it turns out, some principles of psychology that your designers hold dearly can help significantly.
---

How can you get into the head of your audience? How can you know what they look for? How can you get their attention? As it turns out, some principles of psychology that your designers hold dearly can help significantly.

_A lot of this content is sourced from the amazing Jon Yablonski of [Laws of UX](https://lawsofux.com). You should definitely check out his work and buy his book_.

## Using the Brain to Your Benefit

User Experience (UX) design and the science of Human-Computer Interaction (HCI) have a lot to offer marketers, especially in this increasingly digital nature of out marketing efforts. Many of the folks in these fields have done a lot of research into the psychology around how humans use technology. Many of their findings can be used by marketers to aid their positions and promotions.

As we look to increase the effectiveness of our social media marketing efforts, pausing to consider what our designers (wherever they are in your network) believe about users’ engagement with digital products provides invaluable knowledge that can bolster your progress in social meetings. Three laws of UX in particular can aid us in this goal:

* Hick’s Law (Analysis Paralysis)
* Fitts’s Law (Time to Acquire)
* Jakob’s Law (Uniformity of Websites)

These laws are probably better know by what they represent, rather than their names. So I gave them nicknames for the sake of our discussion. Those nicknames are in parentheses.

## Analysis Paralysis

The most troubling thing about painting or drawing for me is not the creation itself, but instead the endless possibilities of a blank canvas. Making that first decision in the midst of infinitude is the most difficult part.

If you’ve ever experienced that feeling, be it in art or anything else, you’ve experienced the sting that defines Hick’s Law, which is defined this way:

> The time it takes to make a decision increases with the number and complexity of choices.  

This has direct application to social media marketing. We already know that the customer has a small attention span, and we spend a lot of time trying to get their attention. So once you have their attention, **minimize the complexity of the call to action (CTA)**. This comes into play with copywriting, the directness of the visual components, and the connectedness of the journey.

An example could be “Reserve your spot now” instead of “Don’t you want to join us?” The assertion makes the decision clear, whereas the question allows doubt and other possibilities to flood the customer’s brain.

In general, if your customer has to stop and think about your CTA, especially in the fast-paced world of social media, you’re on the verge of losing them.

<div class="Image__Small">
  <img
    src="./images/target.jpg"
    title="A target"
    alt="A target dartboard. Photo by Engin Akyurt on Pexels"
  />
</div>

## Time to Acquire

Have you ever watched an ad on TV or elsewhere and at the end, couldn’t tell who the ad was for? Conversely, have you ever watched an ad (Nike does a great job of this) and known who the ad was for within the first one or two seconds?

That “time to acquire” the target, in this case the brand associated with the ad, is the same idea that undergirds Fitt’s Law.

Fitt’s Law is usually used in the context of user interfaces, and finds most of its application in the size of interactive pieces, like buttons and text fields. But the principle is much wider:

> The time to acquire a target is a function of the distance to and size of the target.  

The takeaway for advertisers can be boiled down in a simple statement: **make it big, make it interactive, and make the journey short**.

When [Cultivating Ad Creative that Converts](../cultivating-ad-creative-that-converts), finding a big hero image, or a central piece of content is paramount. Make it big.

But don’t stop there. Make it interactive. Give it a CTA.

Then, most importantly, keep the journey short. Your customer shouldn’t need more than one click or tap to convert. An easy example of this is “Comment below” or “share this with a colleague”. For external conversions, have your customer land on a page or screen that will immediately let them convert on your CTA.

For further help on that last part see the next law.

## Uniformity of Websites

This is best explained by its definition, which I’ve often felt like a slap on the wrist:

> Users spend most of their time on other sites. This means that users prefer your site to work the same way as all the other sites they already know.  

Advertisers can use this by using known defaults for ad layout, like captioning your images, providing a link to a transcript for your video, captioning them. But **a big, easy win for this is a unified design system for ad creative**. Your ad on social media should look like it belongs with the site the users land on, and visa versa.

## Closing the Deal

Leveraging these ideas will help your social media ads leap and bounds. They may not get you the attention of the customer, but they’ll help you reach your ROI when they do.

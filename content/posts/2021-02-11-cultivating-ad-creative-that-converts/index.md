---
title: 🎨 Cultivating Ad Creative that Converts
author: Guy Batton
date: 2021-02-11
hero: ./images/hero.jpg
excerpt: Making ad creative can be exhausting. Canned templates don’t feel genuine enough and custom designers are too expensive. Is there anything that can be done? These tips will help you maximize your effectiveness with your visual treatment to your ads.
---

One of my favorite ads of all time is a collection of one-star reviews. Big, bold letters **There are NO Easy Runs**, with the first name and hometown of the reviewer. The only copy is the review: an unredacted, bright, readable one-star review.

This ad was featured in Adweek’s “Five Stars” (ironic?) the year it was published.

So how did they do it?

## Elements of Excellent Ad Creative

_The spectacular team at Struck designed this campaign. Please check it out here and show them some love: [Snowbird One-Star Campaign](https://www.struck.com/portfolio/snowbird)._

Successful ad creative is a process that takes refining. You might say it’s a **Cadence**.

But there are clear patterns that can be followed and will lead to success. Of course, ad patterns, like the fashion industry, will change their looks over time but the patterns themselves stay the same. As an example, we might put floral print on different pieces of clothing over the years, but floral will likely never go away. It will return as a trend every spring.

Some enduring elements of successful ad creative are:

- Simple, rather than complex
- Focused, rather than multi-faceted
- Surprising and delightful

> Ad patterns, like the fashion industry, will change their looks over time but the patterns themselves will stay the same.

As is my point with most things, the same principles that apply for successful advertising apply in product design, entrepreneurship, and life in general. The best work will be done with simplicity, focus, and delight in mind.

### Savoring Simplicity

The Struck Snowbird ads achieved such success because they were simple. The copy was simple (that’s a blog post of its own). The message was simple. The visuals were simple. The spread was simple.

<div class="Image__Small">
  <img
    src="./images/struck_no_easy_runs.jpeg"
    title="There are No Easy Runs"
    alt="An ad by Struck from their Snowbird One-Star Campaign"
  />
</div>

The magic of the simplicity is tied up in the other two points below. So we can’t separate one from another.

Simplicity for the sake of itself can lead to something meaningless, and advertising starts with **bringing meaning to the buyer.**

_If you want to see some suggestions on how to bring meaning to your buyers, check out my introduction to buyer personas here: [🗺 Marketing UX: Using Customer Journeys for Growth](../marketing-ux:-using-customer-journeys-for-growth-free-persona-template)_

What simplicity gains for the ad is a distilled meaning that can be taken at a glance. It’s the same idea behind the adage of the elevator pitch. That simplicity sets a core for the success that comes from focus and surprise, which is why it comes first.

In the ad from Struck, they present nothing but the mountain and trees. The skier feels minuscule compared to the immense landscape. The photo is so breathtaking that it leaves the reader immediately with a thought: “I don’t care what costs or where it is, I need to see that view.” And the power of the ad is consistent of the distilled simplicity in the image for the spread.

### Focus, Let it Flow Through You

Struck has another ad campaign that earns a spot here. Their hyper-focused Life Elevated campaign for the [Utah Office of Tourism](https://www.struck.com/case-studies/utah-office-of-tourism) is a textbook example of how focus converts.

Looking specifically at their Mighty 5 campaign, we can see the focus of the message with a logo and a focus on the natural colors God gave to Utah. What makes this campaign so striking (no pun intended) is how many ways Struck found to restate the same benefits in different ways.

It worked too. Struck tells us that this campaign had $1.96 billion directly attributed to it.

<div class="Image__Small">
  <img
    src="./images/gift.jpg"
    title="A surprise gift"
    alt="A surprise gift. A Photo by Anastasia Anastasia on Unsplash"
  />
</div>

### Surprise and Delight

I hope to come back and dedicate an entire post to this idea. Surprising the buyer is a surefire way to imprint your message into their minds. One fantastic example of this is in the Alexa-enabled Space Station. Viewers were encouraged to interact with the ad via voice.

By surprising the buyer with innovation, focus, and simplicity, Amazon found a 144% lift in purchase intent, according to the [Internet Advertising Competition](http://www.iacaward.org/iac/winner/17889/truex-amazon-and-rufus-powered-by-initiative-wins-2020-iac-award-for-welcome-to-the-alexa-enabled-space-station.html).

---
title: '🗺 Marketing UX: Using Customer Journeys for Growth [Free Persona Template]'
author: Guy Batton
date: 2021-02-04
hero: ./images/hero.jpg
excerpt: Personas can unlock customer journeys for your brand. But why should I use them?
---

There’s a Hispanic proverb that, when translated to English, goes like this: “Tell me who you walk with, and I’ll tell you who you are.” This proverb is great for living, but can it help us in marketing and design?

One of the greatest axioms of business is that we are often solving our own needs, and yet we are not our users. But User Experience design has recaptured the idea of walking with a user in personas. Personas allow us the ability to pseudo-step into our buyers’ shoes and walk with them. When we see the customer’s journey, we can begin to think and act like them, and thus better meet their needs in the marketplace.

## Enter the Persona

Personas are profiles for your users and buyers that include all kinds of information. Wielded well, they can be a powerful tool for your brand. In this post, you can expect to find **reasons to use personas** and **what they are**.

_By the way, I have a free template for a persona, and I’d love to give it to you! It’s down at the bottom of this page. Feel free to skip down there if that's what
you came here for._

## But What is a Persona?

A persona is like a one-page resume for a customer/user, but instead of focusing on their life’s work and achievements with the goal of getting a job, they’re displayed with the goal of honing our understanding of them. So a typical persona card might have:

- Personal information (name, job title)
- Demographics (race & ethnicity, location, industry, income level)
- Psychographics (family life stage, hobbies, favorite social media networks)

The specifics will of course vary with your product and brand, but personas can (but shouldn’t) include everything imaginable.

## Empathy, Language, and Vision

When I first encountered personas, they seemed a little strange. Often they have a ton of seemingly superfluous information, like hair and eye color. Then I heard the team members call the personas by their names, “Rachel,” “Tommy,” as though they were real people who were also part of the team.

But eventually their importance clicked with me, and now I couldn’t imagine a different strategy.

So what made the difference for me? Three things:

1. Empathy
2. A Common Language
3. Unified Vision

All three of these can be encapsulated in a single statement: know your customer. The crux of why buyer personas and customer journey maps work is because it enables us to know our customers by stepping into their worlds for just a moment, and connecting with them.

<div class="Image__Small">
  <img
    src="./images/ux-indonesia-WCID2JWoxwE-unsplash.jpg"
    title="A Man making a user map by UX Indonesia on Unsplash"
    alt="A Man making a user map by UX Indonesia on Unsplash"
  />
</div>

### Channeling Empathy

It goes without saying that we struggle to relate to what we don’t understand. In other words, you can’t know your customer without experiencing their world.

Personas give us snapshots of what life is like for our users. They help us to understand their journey.

> You can’t know your customer without experiencing their world.

[Don Norman](https://moz.com/blog/personas-understanding-the-person-behind-the-visit), a giant of the User Experience design industry, has said, “A major virtue of personas is the establishment of empathy and understanding the individual who uses the product.” If we want to understand our customers, and best meet their needs, then we can benefit from the virtue of personas.

### Speaking a Common Language

At the team level, personas provide immense value in helping teams to understand one another, as well as the customer. While it took some getting used to, hearing “Rachel” come up in virtually every planning meeting was really impactful. It’s like having acronyms on your team, but instead of representing a process, they represent a person.

When Rachel becomes a part of the team, we know that we understand her. We also become more invested in her successes, and looking out for her best interests is increasingly natural.

Identifying with a persona’s strengths, weaknesses, and challenges will help us find better ways to cater to their needs from a product perspective, and help us advertise our solutions to them.

### Unifying Vision

> If you want to make money, you need a vision centered around your customer.

Keeping goals in front of us is one of the best ways to achieve them. We learn by repetition, and we easily forget things. This is the case with personas too. We can unify our entire brand, product, and marketing (redundant?) strategy around a single handful of people. Tailoring every experience to maximize the joy of our personas is a way to help maximize success through focus of vision.

When you unify your vision around the customer, you steer the entire organization toward success. If you want to make money, you need a vision centered around your customer. Personas enable a clear, visual, reference for everyone to point to and say “This is where we are heading.”

## Take My Persona

Thank you so much for reading through this. I so believe in personas that I want to give you a template to start with. I don’t have subscribing here yet, but when I do, I’ll update so you can sign up for the PDF. In the meantime, you can find where I got my ideas here: [Personas & Messaging Template | Figma](https://www.figma.com/community/file/903445664989818507).

<div class="Image__Small">
  <img
    src="./images/Buyer_Persona.jpg"
    title="Persona Template"
    alt="persona template"
  />
</div>

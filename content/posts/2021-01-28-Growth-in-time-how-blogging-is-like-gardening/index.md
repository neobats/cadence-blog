---
title: 🌱 Growth in Time, How Blogging is like Gardening
author: Guy Batton
date: 2021-01-28
hero: ./images/hero-garden.jpg
excerpt: Blogging can easily become a chore, but it should be a treat. A simple shift in thinking about blogging can produce exponential growth, it just takes time.
---

Justin Mast was born with a green thumb—a fifth-generation green thumb. His family had long been in the business of growing greens. He watched as the ebb and flow
of markets moved [horticulture](https://en.wikipedia.org/wiki/Horticulture) away from the small business and toward big-box retail, where personal care was displaced
by decorated shelves, and expert advice was replaced with inexpensive tags.

So when the opportunity arose to shake up the market, he decided to rustle some leaves.

## Bringing the Gardener Home

Justin found himself running a direct-to-consumer greenhouse, called [Bloomscape](https://bloomscape.com/about/). After they got going, the demand for care quickly outgrew
the pot they'd planted themselves in. So they enlisted help. Justin brought his mom in, a seasoned (and fourth generation) gardener. She took calls on all kinds of questions,
offering advice on plant care and personal problems. Her influence helped the company bloom, and the time came to harvest the fruit. To scale up to the needs of their customers,
Bloomscape began a blog.

They found that a blog allowed them the scale they needed to solve customer questions provide better care. They knew success meant equipping their customers with knowledge,
which required a large inventory of answered questions, helpful tips, and thoughtful guides.

> They knew success meant equipping customers with knowledge

Now they have a digital greenhouse full of leafy nutrition. They have an A–Z care guides, plant care 101, and a blog dedicated to product information and cross-channel marketing.
They accomplished this by starting small, building over time, and letting nature do its work.

_Hey, if you like this story, check out more on this [podcast](https://www.crema.us/people-of-product/70) from my employer.
It's where I got most of my information.
And if you want to partner with people who believe design, technology, and culture can help people thrive,
please consider reaching out to us at [Crema](https://www.crema.us)._

<div class="Image__Small">
  <img
    src="./images/plant-couch.jpg"
    title="Plant Next to a Couch by Matthias Koch on Unsplash"
    alt="a green, leafy plant next to a couch"
  />
</div>

## An Allegory of Blogging

> If you ignore your garden, it will die. And the same goes for your blog.

Blogging can easily become a chore, but it should be a treat. A simple shift in thinking about blogging can produce exponential growth, it just takes time. It should be a joy, much like caring for a garden.

The parallels between a good blog and a garden are many. With patience, regular attendance, and pruning, a blog becomes a beautiful, formidable, and fruitful product all on its own. And like a garden, without proper, regular care, a blog will die over time.

I could drone on about more parallels, but the principle is simple: a blog’s success depends on the time you attend to it. If you ignore your garden, it will die. And the same goes for your blog.

### Time, time, and time again

One of the underpinning ideas behind all advertising is repetition. It **takes time** to acclimate your audience to your brand and message. A blog is no different. In fact, a blog should _always_ be considered a long-term investment.

This is exactly like planting a garden. The process of choosing seeds, watering them, tenderly looking after them, adding more each day; that endless routine is what brings about growth. In the same way, your blog should be viewed as an **investment in the future** catalog of your brand.

There are certainly examples of blogs that sprout and immediately take off with tons of readers, making conversions like magic. But they are in no way the norm, and like most plants that sprout quickly, they lack a good root system. Only time, and careful attendance can bring about the strong foundation a blog needs to help your brand succeed.

Bloomscape understood this from the beginning. By bringing in an expert who started with customer engagement, and then moved into blogging, they cultivated a source of nutrition for their brand that now yields dividends far beyond what they started with.

### Nourishing Yourself and Your Reader

When a client grabs their phone to search for something, they’re often either curious about something new, trying to better understand something they know, or are trying to solve a problem they have. At least, that’s my own experience.

Blogging allows you to become the end and the beginning of the customer journey by meeting their needs before they realize them. Bloomscape added their guides so that they could help existing customers enrich their experience, and also gain new customers by solving questions.

But having a foundation of answered questions takes time. Nourishing your brand takes time, just like a plant. And the same goes for your readers.

> Nourishing your brand takes time, just like a plant.

<div className="Image__Small">
  <img
    src="./images/sprout.jpg"
    title="Sprout"
    alt="Small sprouts coming out of the soil."
  />
</div>

### You Can’t Automate Intentionality

The best gardeners know that automation doesn’t match personal touch. Sure, one can take some fancy tech, stick it in the soil, and let the plants “water themselves,” but physical care is only one part of the plant’s life.

In the same way, your customers need more than raw content for their benefit. Taking the time to write something real, fresh, and personal will meet your reader where they are and relate to them more than our automation can.

Think of it like singing to your plants. For some reason, the time and care we take to sing to them helps them grow more than if we just left them with their “needs.”

## Start and Don’t Stop

There are many parts to blogging successfully. There are entire fields of study on SEO, keywords, title length, and more.

Those things are important. And it’s good to pay attention to them. But “content is king” as they say, and it’s only with time that one can master the content in their industry.

A good blog takes time, repetition, and intentionality to make it thrive. This is exactly like a garden, and it’s the story that Justin Mast discusses in People of Product. Granted, Justin speaks more directly about technology benefiting the connection his company has with its clients, but the principles remain.

If you want to know more about how to make your business thrive with care and technology, reach out to us at [crema.us](https://www.crema.us)

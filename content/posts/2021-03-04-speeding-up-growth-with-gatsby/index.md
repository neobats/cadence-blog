---
title: ⚡ Speeding Up Growth with Gatsby
author: Guy Batton
date: 2021-02-25
hero: ./images/speed-tunnel.jpg
excerpt: If you want to capture your customer, you need a speed that matches or exceeds the speed of their lifestyle. Gatsby provides measurable ROI and allows you to grow your brand by providing this speed out of the box.
---

The best marketing meets people where they are, understanding that they don’t live in isolation. It interrupts and yet feels seamless, blends in but stands out.

There are a million ways to reduce friction in the [customer journey](../marketing-ux:-using-customer-journeys-for-growth-free-persona-template), but one way is with an excellent website. And one platform that excels in reducing friction, delivers on customer expectations, and provides great return-on-investment is [Gatsby.js]<https://www.gatsbyjs.com>).

_Hey, if you can’t tell already, I bet you will soon: I really love Gatsby. I’d be delighted to work with you on your next web project if you think Gatsby may work for you._

There are three main reasons why you should consider Gatsby for your next website platform.

1. Security by default
2. Scale is no problem
3. Speed, speed, speed (this has sub-points. You’ll see.)

Security, scale, and speed matter in your marketing individually and collectively, but speed says the most. When a customer is in their journey, and a micro-moment pops up, that opportunity is passed on by the slow website. In other words, if you want to capture your customer, you need a speed that matches or exceeds the speed of their lifestyle. Gatsby provides measurable ROI and allows you to grow your brand by providing this speed out of the box.

## Security and Scale

Gatsby delivers security and stability to those who use it by distilling the necessary files down into static sites. Static sites are what drove the web in its early days, but now they’re viewed as extremely limited. Gatsby takes advantage of modern tools to enable developers, content creators, and marketers to get their work done quickly and elegantly.

Static sites are necessarily secure, because there aren’t any servers to poke holes in. Since Gatsby makes static sites, they offer the best kind of security: the kind you don’t have to think about.

Such sites are also really scalable. You can take your e-commerce platform and run it for tens or tens of millions of people, if you use some modern deployment techniques for static assets (like your Gatsby site).

## Speed, Speed, and More Speed

But by and far away, the best feature that Gatsby offers to its clients is speed. This speed comes in a variety of flavors.

1. Speed to load
2. Speed to content
3. Speed to market.

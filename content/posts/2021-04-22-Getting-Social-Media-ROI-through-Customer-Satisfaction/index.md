---
title: 😌 Getting Social Media ROI through Customer Satisfaction
author: Guy Batton
date: 2021-04-22
hero: ./images/hero.jpg
excerpt: Measuring long-term benefits from social media can be really difficult. I know it was for me at first. “What do the likes and shares actually gain for my business?” By setting goals and establishing how to measure them, tracking ROI, and gaining value for your business becomes easy.
---

Measuring long-term benefits from social media can be really difficult. I know it was for me at first. “What do the likes and shares actually gain for my business?” By setting goals and establishing how to measure them, tracking ROI, and gaining value for your business becomes easy.

Much has been written about [SMART](https://www.indeed.com/career-advice/career-development/smart-goals) goals and how they focus an aim to make them more real and valuable. Their popularity isn’t for nothing. It’s impossible to measure an aspiration, unless there are metrics tied to it.

The same goes for social media. If you want to measure your success (or failure), you need to derive those measurements from metrics. Let’s look at am **often overlooked metric and how to calculate its ROI.**

## Customer Satisfaction and Social Media

This is my new favorite metric to track with social media. After all, the adage is true that the customer is often right. Often it’s hard to know if your survey responses or personal questions are providing an accurate picture of your how happy your customers are. Crafting unbiased, effective questions can be really cumbersome.

But on social media, you have customers providing you with unfiltered, **open-ended feedback about the experience you provide**. And the best part is that it’s (relatively, more on that later) free! You don’t have to incentivize a response, since the response is freely given.

Additionally, in some social media platforms (specifically on Twitter), a customer’s feedback on their satisfaction or dissatisfaction can become an opportunity for building brand loyalty. By responding to a customer’s complaint, whether mentioning your brand directly or not, with the same incentive you’d give for a survey response, you can help that customer down their journey toward rabid fandom, rather than jaded megaphone.

## Why Track Satisfaction

Tracking customer satisfaction provides a detailed insight to how your brand is perceived by your audience. One major benefit is that this feedback is valuable despite the reach your brand has. Additionally, a customer that is very satisfied will often tell their friends, and increase your reach for you. And on the other hand, a customer’s negative response will be even more likely to impact your reach. Tracking this perception is vital on its own, but can also illuminate other metrics too.

> Don’t detract from qualitative value by forcing it into a quantity.  

While codifying these open-ended responses from customers into a measurable score has value, take a word of caution: don’t detract from the value of qualitative data by forcing it into a quantity. It’s certainly easier to present numbers on a graph to executives, but think about how you can store away a customer’s direct responses, and show the change in themes over time.

<div class="Image__Small">
  <img
    src="./images/spreadsheet.jpg"
    title="A Cherry Blossom Tree"
    alt="A spreadsheet. Photo by Lukas Blazet on Unsplash"
  />
</div>

## How to Track Satisfaction

How does one best track customer satisfaction? I recommend a hybrid approach. But the most important thing is to keep records. Spreadsheets are invaluable. Something like Excel or Google Sheets can work, or you might opt for something like [Airtable](https://airtable.com). The point is, write everything down. But don’t stop there, you need to make sense of the data you have.

I mentioned in the last section that codifying feedback from customers on social media can be valuable, and you should do it. Of course leveraging advanced data patterns like what can be found in data science and analytics can help, but you don’t need them to get started. Try some of the following and see where they take you:

* Group like responses by overall sentiment
* Look for key words that are repeated, and group based on those
* Using those key words, build a scoring system for customer satisfaction
* Lay out responses by where they fall on that scale.

These are some easy ways to quantify the qualitative data that you can find by listening to reactions to your brand on social media. Just make sure to keep the raw responses and who they belong to as well, you will want to see if their sentiment changes over time.

## Measuring ROI of Customer Satisfaction

The math for ROI is simple, but fine-tuning it for your situation can be difficult. First, let’s look at the formula:

```
value gained / (cost invested) * 100 = ROI
```

Those variables are highly dependent on your goals. You can find a bunch of articles online that will tell you all of that, but I haven’t found many that give hard examples. So I thought I’d give you one.

### Determining Valued Gained

The value gained from tracking customer satisfaction may vary depending on where you are in your journey through this listening exercise. If it’s your first time, value is the strength and reliability of the pulse you took from your customers. What do you know about their views of you? Can you aggregate the data into a single sentence summary and a numerical score? If so, you have a really great idea of what your customers think of you. That’s incredible value.

Later on, one example metric that can be tracked is the number of responses. Did it increase or decrease over time? Be sure not to take stock of these changes too quickly, as the team at [Hootsuite](https://blog.hootsuite.com/measure-social-media-roi-business/#How_to_get_ROI_from_social_media) suggests. Sometimes it may take months to fully realize the value gained from a social media campaign.

> Protip: try pairing the change in Customer Satisfaction with another metric, like post engagement or reach.  

### Calculating Cost Invested

While most social media platforms are free, many of the tools we use to make marketing easier are not. Everything has a cost, and it’s important to measure your costs carefully.

The cost of listening to your customers, responding to their public opinions of your brand, and tracking/processing those responses can be weighed against alternatives. What would you do instead to track customer satisfaction? Maybe you’d make a survey, or pay for a third-party call center for customer service. Those are costs that come in, and they can directly affect your bottom line too! In effect, our question becomes, does [[🏯 Growing Brands Through Good Nature: Social Listening]] provide long-term value?

### Generating ROI

Once you have determined the value gained, and measured your associated costs, you’re ready to do the math.

Lets say your value is “Satisfaction score increase by 15%” and the cost is “25% reduction in customer complaints in direct messages”, then your ROI might be calculated like this:

```
15 / (1 / 25) * 100 = ...
```

That `1 / 25` is because of the _reduction_.
If we take these and get the result, we end up with 37,500% increase in score per DM complaint! That would be a spectacular ROI. Something like that may never happen, but that’s a metric you can take to your stakeholders.

### Final notes

One thing I really like about using Customer Satisfaction for tracking ROI in social media is that it illuminates the business case for social media marketing beyond revenue. Of course the ultimate goal in all marketing efforts is to drive profit, and the most straightforward path to that profit is by driving sales. But there are other ways to win in marketing, and Customer Satisfaction is one of them.

---
title: 🔍 Magnifying Impact with Search Keywords
author: Guy Batton
date: 2021-02-18
hero: ./images/hero.jpg
excerpt: Keywords may be one of the most common parts of digital advertising for beginners, but that doesn’t make them unimportant. Matching copy to keywords can help you maximize your ad effectiveness, and grow your business.
---

> Everything starts from a dot.  
> —Wassily Kandinsky

When the Bauhaus school took its ideas of art and applied them to everyday things, they sparked a revolution in thought behind creative endeavors of all types. Kandinsky famously summarized these thoughts in a book, [Point and Line to Plane](https://www.amazon.com/Point-Line-Plane-Wassily-Kandinsky/dp/1614275467). One takeaway quote is his idea that “Everything starts from a dot.” In digital marketing, that dot is extended to a tap or click.

The whole customer journey often begins with a click. From that click comes an endless world of possibilities, and it’s our job as marketers to capture that journey and bring benefit to our customers. One of the best tools in the marketer’s kit is **paid search**. Paid search is best achieved through **keywords** and **matching**.

Keywords may be one of the most common parts of digital advertising for beginners, but that doesn’t make them unimportant. Matching copy to keywords can help you maximize your ad effectiveness, and grow your business.

Bauhaus taught us more than dots. The teaching brings the axiom of “form follows function,” which freed craft artists and architects to design without qualifications around elaborate decoration, and distill the art to its essential duties. _Search ad copy can leverage these same ideas to magnify impact._

<div class="Image__Small">
  <img
    src="./images/map.jpg"
    title="Selecting Keywords"
    alt="A map photo by Sylwia Bartyzel on Unsplash"
  />
</div>

## Selecting Keywords

If a dot is the start of a line, and a tap is the start of a journey, then the keyword is the start of the ad. Keywords are the **foundation of your ad**. They directly drive the success or failure of your campaign. So it’s paramount that you pick keywords that fit your goals and strategy.

Google gives some [tips](https://support.google.com/google-ads/answer/2453981) for how to select keywords that make a difference. Here are some of the top findings:

- Focus your goal into a few phrases, then set those phrases aside
- From that list of phrases or words, decide how your ad will be used
- Chop down that list into a few (at least 3) words or phrases

There’s a lot in there, so let’s break it down:

### Focus Your Goal

Here at Cadence we believe that all marketing, design, and entrepreneurship comes down to a journey. When creating ad campaigns, that journey should be front and center. Another way to look at it is as a problem and a solution. This strategy is particularly effective with search campaigns.

When you customer searches for something, they’re about to be—or already are—in a state of displeasure or dissatisfaction somehow. To put it differently, I only search for a new backpack when I’m dissatisfied with the one I have. If I see an ad for an Ogio backpack that meets my needs, I’ve been given a solution. That’s an effective marketing campaign. The solution enters the searcher’s problem, and that happens best through paid search ads. Achieving successful ad campaigns starts with good keywords that are focused on that solution.

> The solution enters the searcher’s problem, and that happens best through paid search ads.

### Decide How the Ad will be Used

Bauhaus told us that form should follow function, but it didn’t create styrofoam cups. They created useful products that focused on utility, without being so generic that they lose their beauty.

Successful ads are the same way. They take the journey and pick one part of that to leverage their best potential. It’s a fancy way of saying “You can’t make everyone happy.”

As an example, pick your ad to be used either for bringing in lots of new leads, or focus it to maximize impact with a niche. Either way, your keywords will drive how your ad is used, and so you should pick your goal beforehand.

### Pick a Few Words

Keywords are most effective in small groups. It seems that 3 is a good number to aim for, but Google doesn’t seem to mind more than 3. The keywords don’t need to be single words, but they can be. An example of a good set of keywords for an ad about Cadence might be:

- tech product design
- digital advertising blog
- marketing startup

<div class="Image__Small">
  <img
    src="./images/piet-mondrian.jpg"
    title="A Piet Mondrian Painting"
    alt="A painting on display by Piet Mondrian. Photo by Ernest Ojeh on Unsplash"
  />
</div>

## Matching Copy and Keywords

To continue with the Bauhaus metaphor, copy and keywords go together like form and function. That is to say that keywords determine the _function_ of the ad, and copy drives the _form_ of the ad.

You simply cannot have a successful search ad without copy that matches your keywords.

Google uses [Olay](https://www.thinkwithgoogle.com/marketing-strategies/search/search-behavior-experience/) as an example of this. They kept the keywords the same, but they adjusted the copy of their ad to match those keywords and found much greater success. In this case it was a simple change to “What Causes Under Eye Circles?” that made all the difference. Be sure to check out their case study for more details.

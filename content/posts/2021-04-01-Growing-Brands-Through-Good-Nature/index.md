---
title: '🏯 Growing Brands Through Good Nature: Social Listening'
author: Guy Batton
date: 2021-04-01
hero: ./images/hero.jpg
excerpt:  Active listening isn’t just a good practice in-person, it’s essential to a full-fledged social media strategy. How can social listening impact your brand growth?
---

Sometimes it’s hard to imagine what marketing was like before social media. Personally, I didn’t get into marketing until social media platforms were deeply integrated into everyday life, and ads were an assumed part of the experience. The landscape has changed so much that the idea of reaching customers without social media seems impossible.

How did companies market themselves before social media? I believe they did a lot of the same things that brands do now. One of the biggest impacts a brand can make is leveraging a tactic as old as humanity itself: Listening.

It turns out that one of the best practices for building relationships in person is also extremely effective—even vital—in digital relationships. I’m going to walk through a couple examples of how listening has been influential in marketing past, why it’s so effective, and how you can leverage social listening to build your brand relationships today.

<div class="Image__Small">
  <img
    src="./images/japan.jpg"
    title="A Cherry Blossom Tree"
    alt="A Japanese Cherry Tree in Blossom.. Photo by Bagus Pangestu on Pexels"
  />
</div>

## A History Lesson

Some companies, like IBM, Exxon Mobil, and Ford, are considered old here the US. Some companies are old in their own regards, because of the age of their industry. But in Japan, old takes on an entirely different meaning.

Japan has so many companies that are more than 100 years old, they have a name for such companies: [Shinise](https://en.wikipedia.org/wiki/Shinise). The Shinise are companies that are typically at least 100 years old, but some are more than 500 years old. There’s a secret to that kind of longevity; a secret that obviously stands through major technological, environmental, market, and social shifts.

There are several reasons that play into the longevity of the Shinise, and one of them is a radical dedication to customer service—to listening. BBC’s [Worklife](https://en.wikipedia.org/wiki/Shinise) covered this idea with some depth. They asked someone at Kyoto University his opinion on the unique situation in Japan’s markets. One of the answers is *omotenashi* which is a style of customer service that strives to anticipate what customers need.

One particular family-run inn in Kyoto describes their work in this way:

> Heart-to-heart communication—that is the best part.  
> —Akemi Nishimura  

Nishimura goes on to say that her inn has an 80-year old handbook that details how to run the style of inn, including what to do with a guest’s handkerchief. She outlines the steps that are considered best practice and then qualifies with the importance of **listening**: “But some customers wouldn’t like that… you should ask permission beforehand.”

It’s no secret, then that these businesses have lasted so long when they prioritize their customer’s needs above all else, and listen to them.

### Lessons in Listening

Active listening is considered one of the best skills [business leaders can learn](https://www.entrepreneur.com/article/354104). Its effectiveness is summarized well in an old proverb: “Even fools are thought wise if they keep silent, and discerning if they hold their tongues.” Proverbs 17:28 (NIV)

What we can learn from some of the Shinise is that while we may change our methods of doing so, we cannot ever stop listening. We may listen via Twitter threads and Instagram reposts instead of the looks on faces when a handkerchief is folded, **but the act of listening doesn’t change**.

## Tactics for Social Listening

So how should one go about listening to their customers on social media? Here are three (3) tips:
*much of this is inspired by information found here, from [Brandwatch](https://www.entrepreneur.com/article/354104).*

### Gather a list of important metrics

You can call them KPIs or whatever you want, but these are the crux of the listening. In order to know that you’re achieving something with your listening, and not just taking in sound, you need something to measure against.

Some common ideas for things to measure include:

* customer service
* crisis management (this will likely be a long-term measure)
* ad campaign performance

These will help you establish your goals for the listening. Next, you need to get into the conversation, and listen in on your customers.

### Listen and record

Take your goals and narrow them, figure out the best way to measure them based on what your customers say and do.

An example of this could be tracking the response time of Twitter followers asking for help. You could track the response team of your account manager/team member to the customer, and you can also track the customer’s response time to your replies. Both of these will be helpful metrics to determine where there are strengths and gaps in your customer service model.

### Adapt, improve, and repeat

As with anything else in marketing, social listening is an “infinite game.” It never ends. We simply tee up and play again. [The same goes for our relationships with our customers.](https://www.cadence.surf/building-relationships-through-email-marketing)

The Shinise teach us that listening and adapting are part of the process too. If we do nothing with the insights we gain, we will never achieve the kind of longevity that they possess. It is as Nishimura put it: we cannot only pay attention to the handkerchief, we must also pay attention to the person it came from.
